﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Parking;

namespace Program
{
    class Program
    {
        private static ParkingLot parkingLot;

        static void Main(string[] args)
        {
            //Init
            Init();

            //Step 1.0 - Sequence diagram - regular Park
            try
            {
                parkingLot.Park(new Vehicle(10));
            }
            catch (NoSpaceException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (VehicleAlreadyParkedException e)
            {
                Console.WriteLine(e.Message);
            }
            Console.Write(parkingLot.Vacancy);
            Console.Read();

        }

        static void Init()
        {
            parkingLot = new ParkingLot(100);
            parkingLot.Hire(new Valet("Bob"));
        }
    }
}
