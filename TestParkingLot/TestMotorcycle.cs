﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Parking;
using Parking.Vehicles;

namespace TestMotorcycle
{
    [TestClass]
    public class TestMotorcycle
    {
        private double capacity;
        private ParkingLot parkingLot = null;
        private Motorcycle motorcycle = null;

        /// <summary>
        /// This test method checks after the first motorcycle parked, the parking vacancy is up to date
        /// Sequence diagram 1.0
        /// </summary>
        [TestMethod]
        public void Park_MotorCycle_Success()
        {
            //given
            this.capacity = 100;
            this.parkingLot = new ParkingLot(capacity);
            this.parkingLot.Hire(new Valet("Bob"));

            this.motorcycle = new Motorcycle(5, "Harley");
            double vacancyExpected = 95;
            double vacancyActual = 0;

            //when
            parkingLot.Park(this.motorcycle);
            vacancyActual = parkingLot.Vacancy;

            //then
            Assert.AreEqual(vacancyExpected, vacancyActual);
        }
    }
}
