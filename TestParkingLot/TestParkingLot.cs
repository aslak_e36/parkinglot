﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Parking;

namespace TestParkingLot
{
    [TestClass]
    public class TestParkingLot
    {
        private double capacity;
        private ParkingLot parkingLot = null;
        private Vehicle vehicle = null;

        [TestInitialize]
        public void init()
        {

        }

        /// <summary>
        /// This test method checks after the first car parked, the parking vacancy is up to date
        /// Sequence diagram 1.0
        /// </summary>
        [TestMethod]
        public void Park_SequenceDiagrammeRegularPark_Success()
        {
            //given
            this.capacity = 100;
            this.parkingLot = new ParkingLot(capacity);
            this.parkingLot.Hire(new Valet("Bob"));
            this.vehicle = new Vehicle(10);
            double vacancyExpected = 90;
            double vacancyActual = 0;

            //when
            parkingLot.Park(this.vehicle);
            vacancyActual = parkingLot.Vacancy;

            //then
            Assert.AreEqual(vacancyExpected, vacancyActual);
        }

        /// <summary>
        /// This method will test if a car is already parked
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(VehicleAlreadyParkedException))]
        public void Park_VehicleAlreadyParked_Exception()
        {
            //given
            this.vehicle = new Vehicle(10);
            this.parkingLot = new ParkingLot(400);
            this.parkingLot.Hire(new Valet("Mishiali"));
            this.parkingLot.Park(vehicle);
            //when
            this.parkingLot.Park(vehicle);
            //then

        }

        /// <summary>
        /// This test method checks if the parkinglot is full
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NoSpaceException))]
        public void Park_FullParkingLot_Exception()
        {

            //GIVEN
            this.capacity = 0;
            this.parkingLot = new ParkingLot(capacity);
            this.parkingLot.Hire(new Valet("Bob"));

            this.vehicle = new Vehicle(10);
            //WHEN
            this.parkingLot.Park(vehicle);
        }
        /// <summary>
        ///  This test method checks if the parkinglot has enough space for the current vehicle that we want to park
        /// </summary>
        [TestMethod]

        [ExpectedException(typeof(NoSpaceException))]
        public void Park_InsufficentSpaceForVehicle_Exception()
        {
            //GIVEN
            this.capacity = 10;
            this.parkingLot = new ParkingLot(capacity);
            this.parkingLot.Hire(new Valet("Bob"));
            this.vehicle = new Vehicle(11);
            //WHEN
            this.parkingLot.Park(vehicle);
        }

        [TestCleanup]
        public void cleanUp()
        {
            this.capacity = 0;
            this.parkingLot = null;
            this.vehicle = null;
        }
    }
}
