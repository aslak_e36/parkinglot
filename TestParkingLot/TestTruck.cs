﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Parking;
using Parking.Vehicles;

namespace TestTruck
{
    [TestClass]
    public class TestTruck
    {
        private double capacity;
        private ParkingLot parkingLot = null;
        private Truck truck = null;

        /// <summary>
        /// This test method checks for park a truck
        /// Sequence diagram
        /// </summary>
        [TestMethod]
        public void Park_Truck_Success()
        {
            //given
            this.capacity = 100;
            this.parkingLot = new ParkingLot(capacity);
            this.parkingLot.Hire(new Valet("Bob"));
            this.truck = new Truck(40);
            double vacancyExpected = 60;
            double vacancyActual = 0;

            //when
            parkingLot.Park(truck);
            vacancyActual = parkingLot.Vacancy;

            //then
            Assert.AreEqual(vacancyExpected, vacancyActual);
        }
    }
}
