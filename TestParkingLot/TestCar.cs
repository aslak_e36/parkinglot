﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Parking;
using Parking.Vehicles;

namespace TestCar
{
    [TestClass]
    public class TestCar
    {
        private double capacity;
        private ParkingLot parkingLot = null;
        private Car car = null;
        /// <summary>
        /// This test method checks if we can park a Car
        /// </summary>
        [TestMethod]
        public void Park_Car_Success()
        {
            //given
            this.capacity = 100;
            this.parkingLot = new ParkingLot(capacity);
            this.parkingLot.Hire(new Valet("Bob"));
            this.car = new Car(10, "red");
            double vacancyExpected = 90;
            double vacancyActual = 0;

            //when
            parkingLot.Park(this.car);
            vacancyActual = parkingLot.Vacancy;

            //then
            Assert.AreEqual(vacancyExpected, vacancyActual);
        }
    }
}
