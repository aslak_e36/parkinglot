﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Parking;

namespace TestValets
{
    [TestClass]
    public class TestValets
    {
        /// <summary>
        /// This test method checks if the valet availability is false when Valet is driving
        /// Sequence diagram 1.4
        /// </summary>
        [TestMethod]
        public void Drive_ValetAvailable_Success()
        {
            //given
            Vehicle vehicle = new Vehicle(10);
            Valet valet = new Valet("Bob");
            //a valet is by default available just after instanciation
            bool actualAvailability = true;

            //when
            valet.Drive(vehicle);
            actualAvailability = valet.Available;

            //then
            Assert.IsTrue(valet.Available);
        }

        /// <summary>
        /// This test method checks if the valet availability is true when Valet is not driving
        /// </summary>
        [TestMethod]
        public void Park_ValetAvailable_Success()
        {
            //given
            ParkingLot parkingLot = new ParkingLot(500);
            Vehicle vehicle = new Vehicle(10);
            Valet valet = new Valet("Bob");
            //a valet is by default available just after instanciation
            bool actualAvailability = true;

            //when
            parkingLot.Hire(valet);
            parkingLot.Park(vehicle);
            actualAvailability = valet.Available;

            //then
            Assert.IsTrue(valet.Available);
        }
    }
}
