﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Parking;

namespace TestVehicles
{
    [TestClass]
    public class TestVehicle
    {
        /// <summary>
        /// This test method checks if the vehicle size accessor gets the correct value expected
        /// Sequence diagram 1.1
        /// </summary>
        [TestMethod]
        public void Size_Accessor_Success()
        {
            //given
            double expectedSize = 10;
            Vehicle vehicle = new Vehicle(expectedSize);
            double actualSize = 0;

            //when
            actualSize = vehicle.Size;

            //then
            Assert.AreEqual(expectedSize, actualSize);
        }
    }
}
