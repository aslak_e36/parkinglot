﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parking
{
    public class Vehicle
    {
        protected double size;
        private bool isTurnedOn;

        public Vehicle(double size)
        {
            this.size = size;
        }

        public void TurnOn()
        {
            this.isTurnedOn = true;
        }
        public void TurnOff()
        {
            this.isTurnedOn = false;
        }

        public double Size { get => this.size; }
    }
}
