﻿using System;
using System.Runtime.Serialization;

namespace Parking
{
    [Serializable]
    public class NoValetsAvailableException : Exception
    {
        public NoValetsAvailableException()
        {
        }

        public NoValetsAvailableException(string message) : base(message)
        {
        }

        public NoValetsAvailableException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NoValetsAvailableException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}