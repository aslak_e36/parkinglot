﻿using System;
using System.Runtime.Serialization;

namespace Parking
{
    [Serializable]
    public class VehicleAlreadyParkedException : Exception
    {
        public VehicleAlreadyParkedException()
        {
        }

        public VehicleAlreadyParkedException(string message) : base(message)
        {
        }

        public VehicleAlreadyParkedException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected VehicleAlreadyParkedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}