﻿using System;
using System.Runtime.Serialization;

namespace Parking
{
    [Serializable]
    public class VehicleIsNotParkedException : Exception
    {
        public VehicleIsNotParkedException()
        {
        }

        public VehicleIsNotParkedException(string message) : base(message)
        {
        }

        public VehicleIsNotParkedException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected VehicleIsNotParkedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}