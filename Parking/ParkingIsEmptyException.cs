﻿using System;
using System.Runtime.Serialization;

namespace Parking
{
    [Serializable]
    public class ParkingIsEmptyException : Exception
    {
        public ParkingIsEmptyException()
        {
        }

        public ParkingIsEmptyException(string message) : base(message)
        {
        }

        public ParkingIsEmptyException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ParkingIsEmptyException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}