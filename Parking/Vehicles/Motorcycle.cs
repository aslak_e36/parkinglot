﻿using System;
using System.Collections.Generic;
using System.Text;
using Parking;

namespace Parking.Vehicles
{
    public class Motorcycle : Vehicle
    {
        private string brand;
        public Motorcycle(double size, string brand) : base (size)
        {
            this.brand = brand;
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public string Brand { get => this.brand; }
    }
}
