﻿using System;
using System.Collections.Generic;
using System.Text;
using Parking;

namespace Parking.Vehicles
{
    public class Car : Vehicle
    {
        private string color;
        public Car(double size, string color) : base(size)
        {
            this.color = color;
        }

        public string Color { get => this.color; }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
