﻿using System;
using System.Collections.Generic;

namespace Parking
{
    public class ParkingLot
    {
        private double capacity;
        private double vacancy;
        private List<Valet> valets;
        private List<Vehicle> parkedVehicles;



        public ParkingLot (double capacity)
        {
            this.capacity = capacity;
            this.vacancy = this.capacity;
            parkedVehicles = new List<Vehicle>();
            valets = new List<Valet>();
        }



        public void Park(Vehicle vehicle)
        {
            if (IsParkFull()) throw new NoSpaceException("Le parking est complet");
            CheckVacancy(vehicle.Size);
            if (IsVehicleParked(vehicle)) throw new VehicleAlreadyParkedException("Ce véhicule est déjà parqué."); 
            this.vacancy -= vehicle.Size;

            Valet valet = this.ChooseValet();
            if (valet != null)
            {
                valet.Drive(vehicle);
                PutVehicleInPark(vehicle);
            }
        }

        public void Vacate(Vehicle vehicle)
        {
            if (IsParkEmpty()) throw new ParkingIsEmptyException("Le parking est vide, il n'y a aucun véhicule pour le moment.");
            if (!IsVehicleParked(vehicle)) throw new VehicleIsNotParkedException("Ce véhicule n'est pas dans le parking.");

            Valet valet = this.ChooseValet();
            if (valet != null)
            {
                valet.Drive(vehicle);
                RemoveVehicleFromPark(vehicle);
            }
        }

        public void Hire(Valet valet)
        {
            this.valets.Add(valet);
        }

        public void Fire(Valet valet)
        {
            this.valets.Remove(valet);
        }



        private bool IsParkFull()
        {
            if (this.vacancy == 0)
            {
                return true;
            }
            return false;
        }

        private bool IsParkEmpty()
        {
            if (this.parkedVehicles.Count == 0)
            {
                return true;
            }
            return false;
        }

        private void PutVehicleInPark(Vehicle vehicle)
        {
            parkedVehicles.Add(vehicle);
        }

        private void RemoveVehicleFromPark(Vehicle vehicle)
        {
            parkedVehicles.Remove(vehicle);
        }

        private Valet ChooseValet()
        {
            foreach (Valet valet in valets)
            {
                if (valet.Available)
                {
                    return valet;
                }
                throw new NoValetsAvailableException("Aucun valet n'est pour le moment dispnible");
            }
            throw new NoValetsAvailableException("Aucun valet n'est pour le moment dispnible"); ;
        }

        private bool IsVehicleParked(Vehicle vehicle)
        {
            return this.parkedVehicles.Contains(vehicle);
        }

        private void CheckVacancy(double size)
        {
            if (size > this.vacancy) throw new NoSpaceException("Il n\'a plus de place dans le parking pour ce véhicule, place vacante : " + this.vacancy + ".");
        }



        public double Vacancy
        {
            get => this.vacancy;
        }
    }
}
