﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parking
{
    public class Valet
    {
        private string name;
        private bool available;

        public Valet(string name)
        {
            this.name = name;
            this.available = true;
        }

        public void Drive(Vehicle vehicle)
        {
            this.available = false;
            vehicle.TurnOn();
            vehicle.TurnOff();
            this.available = true;
        }

        public bool Available { get => this.available; }
    }
}
