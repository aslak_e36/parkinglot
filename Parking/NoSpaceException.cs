﻿using System;
using System.Runtime.Serialization;

namespace Parking
{
    [Serializable]
    public class NoSpaceException : Exception
    {
        private char v;

        public NoSpaceException()
        {
        }

        public NoSpaceException(char v)
        {
            this.v = v;
        }

        public NoSpaceException(string message) : base(message)
        {
        }

        public NoSpaceException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NoSpaceException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}